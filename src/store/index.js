import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    list: []
  },
  mutations: {
    'AddProduct': (state, payload) => {
      state.list.push(payload)
    },
  },
  actions: {
  },
  modules: {
  }
})
